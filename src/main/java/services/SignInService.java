package services;

import dto.SignInForm;
public interface SignInService {

    boolean isExists(String email, String password);

}
