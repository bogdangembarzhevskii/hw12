package services;

import dto.SignUpForm;
import repository.AccountsRepository;

public class SignInServiceImpl implements  SignInService{

    private AccountsRepository accountsRepository;

    public SignInServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public boolean isExists(String email, String password){
        return accountsRepository.isExists(email, password);
    }

}
