package repository;

import dto.SignUpForm;

public interface AccountsRepository {
    void save(SignUpForm form);
    boolean isExists(String email, String password);
}
